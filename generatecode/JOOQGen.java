import org.jooq.codegen.GenerationTool;
import org.jooq.meta.jaxb.Configuration;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 *
 * @author lybn
 */
public class JOOQGen {

    public static void main(String[] args) throws Exception {
        InputStream in = new FileInputStream("generatecode/configuration.xml");
        Configuration conf = GenerationTool.load(in);
        GenerationTool.generate(conf);

//        int port = Config.getIntParam("rest_redis","port");
//        String host = Config.getParam("rest_redis","host");
//        Jedis jedis = new Jedis(host,port);
//        System.out.println("Connection to server sucessfully");
//        System.out.println("Server is running: "+jedis.ping());
        //set the data in redis string
        // store data in redis list
//        jedis.rpush("KEY_A", "Redis");
//        jedis.rpush("KEY_A", "Mongodb");
//        jedis.rpush("KEY_A", "Mysql");
        // Get the stored data and print it
//        String a = jedis.get("KEY_A");
//        System.out.println(a);
//        List<String> list = jedis.lrange("KEY_A", 0, -1);
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
//        }
    }
}
