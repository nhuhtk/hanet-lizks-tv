package lizks.tv.controller;

import lizks.tv.database.RedissonConnection;
import lizks.tv.db.tables.Collection;
import lizks.tv.model.CollectionModel;
import lizks.tv.model.GenreModel;
import lizks.tv.model.SingerModel;
import lizks.tv.model.SongModel;
import lizks.tv.object.CollectionObject;
import lizks.tv.object.GenreObject;
import lizks.tv.object.SingerObject;
import lizks.tv.object.SongObject;
import lizks.tv.util.Utils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static lizks.tv.util.Constant.*;

/**
 * @author hoang
 * created on 8/4/2020
 * inside the package - controller
 */
public class LizksTVController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(LizksTVController.class);

    public static String PREFIX_API_HOT_SONGS = "/hot_songs";
    public static String PREFIX_API_HOT_SINGERS = "/hot_singers";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handle(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handle(req, resp);
    }

    private void handle(HttpServletRequest req, HttpServletResponse resp) {
        try {
            Utils.logRequest(req, LOGGER);
            processs(req, resp);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
    private void processs(HttpServletRequest req, HttpServletResponse resp) {
        try {
            String content;
            String pathInfo = req.getPathInfo() == null ? "" : req.getPathInfo();
            switch (pathInfo) {
                case "/hot-songs":
                    content = getHotSongList(req, resp);
                    break;
                case "/hot-singers":
                    content = getHotSingerList(req, resp);
                    break;
                case "/hot-collections":
                    content = getHotCollectionList(req, resp);
                    break;
                case "/hot-genres":
                    content = getHotGenreList(req, resp);
                    break;
                case "/list-song-by-singer-id":
                    content = getSongListBySingerId(req, resp);
                    break;
                case "/list-song-by-genre-id":
                    content = getSongListByGenreId(req, resp);
                    break;
                case "/list-song-by-collection-id":
                    content = getSongListByCollectionId(req, resp);
                    break;
                case "/karaoke-search":
                    content = querySearch(req, resp);
                    break;
                default:
                    content = Utils.respNotFound();
            }
            Utils.out(content, resp);

            LOGGER.info(Utils.createRequestLog(req,resp,content));
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private String querySearch(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            int page = req.getParameter(PARAM_PAGE) != null ? Integer.parseInt(req.getParameter(PARAM_PAGE)) : 1;;
            int limit = req.getParameter(PARAM_LIMIT) != null ? Integer.parseInt(req.getParameter(PARAM_LIMIT)) : 10;
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            String param = "";
            if(sign.isEmpty() || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            String query = req.getParameter(PARAM_QUERY_SEARCH) != null ? req.getParameter(PARAM_QUERY_SEARCH) : "";
            if (query.isEmpty()) {
                return Utils.respParamInvalid();
            }
            param = param + query;

            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<Object> songRecords = SongModel.getInstance().querySearch(query,page,limit);
            for (Object object : songRecords) {
                if (object instanceof SongObject) {
                    Map map = new HashMap();
                    SongObject song = (SongObject) object;
                    map.put(PARAM_ID, song.getId());
                    map.put(PARAM_NAME, song.getFullname());
                    map.put(PARAM_PATH, song.getPath());
                    map.put(PARAM_NAME_SINGER, song.getSinger_name());
                    map.put(PARAM_THUMBNAIL, song.getThumbnail());
                    songList.add(map);
                } else if (object instanceof SingerObject) {
                    SingerObject singer = (SingerObject) object;
                    Map map = new HashMap();
                    map.put(PARAM_ID, singer.getId());
                    map.put(PARAM_NAME, singer.getFullname());
                    map.put(PARAM_PATH, singer.getPath());
                    songList.add(map);
                }
            }
            data.put(PARAM_SONG, songList);
            data.put(PARAM_PATH_SONG,LINK_IMAGE_SONG);
            data.put(PARAM_PATH_SINGER,LINK_IMAGE_SINGER);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }

    private String getHotSongList(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            String param = "";
            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<SongObject> songRecords = SongModel.getInstance().getHotSongs();
            for (SongObject song : songRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, song.getId());
                map.put(PARAM_NAME, song.getFullname());
                map.put(PARAM_PATH, song.getPath());
                map.put(PARAM_NAME_SINGER, song.getSinger_name());
                map.put(PARAM_THUMBNAIL, song.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_SONG, songList);
            data.put(PARAM_PATH_SONG, LINK_IMAGE_SONG);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getHotSingerList(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            String param = "";
            if(sign.equalsIgnoreCase("") || timestamp.equalsIgnoreCase("")|| deviceId.equalsIgnoreCase("")){
                return Utils.respParamInvalid();
            }
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> singerList = new ArrayList<>();
            List<SingerObject> singerRecords = SingerModel.getInstance().getHotSingers();
            for (SingerObject singer : singerRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, singer.getId());
                map.put(PARAM_NAME, singer.getFullname());
                map.put(PARAM_PATH, singer.getPath());
                singerList.add(map);
            }
            data.put(PARAM_SINGER, singerList);
            data.put(PARAM_PATH_SINGER, LINK_IMAGE_SINGER);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getHotCollectionList(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            String param = "";
            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<CollectionObject> collectionRecords = CollectionModel.getInstance().getHotCollections();
            for (CollectionObject genre : collectionRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, genre.getId());
                map.put(PARAM_NAME, genre.getName());
                map.put(PARAM_PATH, genre.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_COLLECTION, songList);
            data.put(PARAM_PATH_COLLECTION, LINK_IMAGE_COLLECTION);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getHotGenreList(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            String param = "";
            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<GenreObject> GenreRecords = GenreModel.getInstance().getHotGenres();
            for (GenreObject genre : GenreRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, genre.getId());
                map.put(PARAM_NAME, genre.getName());
                map.put(PARAM_PATH, genre.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_GENRE, songList);
            data.put(PARAM_PATH_GENRE, LINK_IMAGE_GENRE);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getSongListBySingerId(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";;
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";;
            int page = req.getParameter(PARAM_PAGE) != null ? Integer.parseInt(req.getParameter(PARAM_PAGE)) : 1;;
            int limit = req.getParameter(PARAM_LIMIT) != null ? Integer.parseInt(req.getParameter(PARAM_LIMIT)) : 10;
            String param = "";
            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            String singerId = req.getParameter(PARAM_SINGER_ID) != null ? req.getParameter(PARAM_SINGER_ID) : "";
            if (singerId.equalsIgnoreCase("")) {
                return Utils.respParamInvalid();
            }
            param = param + singerId;
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<SongObject> songRecords = SongModel.getInstance().getSongListBySingerId(Integer.parseInt(singerId), page, limit);
            for (SongObject song : songRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, song.getId());
                map.put(PARAM_NAME, song.getFullname());
                map.put(PARAM_PATH, song.getPath());
                map.put(PARAM_NAME_SINGER, song.getSinger_name());
                map.put(PARAM_THUMBNAIL, song.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_SONG, songList);
            data.put(PARAM_PATH_SONG, LINK_IMAGE_SONG);
            data.put(PARAM_PATH_SINGER, LINK_IMAGE_SINGER);
            return Utils.respOK(data);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getSongListByGenreId(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";
            int page = req.getParameter(PARAM_PAGE) != null ? Integer.parseInt(req.getParameter(PARAM_PAGE)) : 1;;
            int limit = req.getParameter(PARAM_LIMIT) != null ? Integer.parseInt(req.getParameter(PARAM_LIMIT)) : 10;
            String param = "";
            System.out.println(timestamp);
            System.out.println(deviceId);
            System.out.println(sign);

            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            String genreId = req.getParameter(PARAM_GENRE_ID) != null ? req.getParameter(PARAM_GENRE_ID) : "";
            if (genreId.equalsIgnoreCase("")) {
                return Utils.respParamInvalid();
            }
            param = param + genreId;
            System.out.println(Utils.genSignKey(deviceId,timestamp,param));
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<SongObject> songRecords = SongModel.getInstance().getSongListByGenreId(Integer.parseInt(genreId), page, limit);
            for (SongObject song : songRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, song.getId());
                map.put(PARAM_NAME, song.getFullname());
                map.put(PARAM_PATH, song.getPath());
                map.put(PARAM_NAME_SINGER, song.getSinger_name());
                map.put(PARAM_THUMBNAIL, song.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_SONG, songList);
            data.put(PARAM_PATH_SONG, LINK_IMAGE_SONG);
            data.put(PARAM_PATH_GENRE, LINK_IMAGE_GENRE);
            return Utils.respOK(data);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    private String getSongListByCollectionId(HttpServletRequest req, HttpServletResponse resp) {
        Utils.prepareHeader(resp, Utils.HEADER_JS);
        JSONObject data = new JSONObject();
        try {
            String sign =  req.getParameter(PARAM_SIGN) != null ? req.getParameter(PARAM_SIGN) : "";
            String timestamp = req.getParameter(PARAM_TIMESTAMP) != null ? req.getParameter(PARAM_TIMESTAMP) : "";
            String deviceId = req.getParameter(PARAM_DEVICE_ID) != null ? req.getParameter(PARAM_DEVICE_ID) : "";
            int page = req.getParameter(PARAM_PAGE) != null ? Integer.parseInt(req.getParameter(PARAM_PAGE)) : 1;;
            int limit = req.getParameter(PARAM_LIMIT) != null ? Integer.parseInt(req.getParameter(PARAM_LIMIT)) : 10;
            String param = "";
            System.out.println(timestamp);
            System.out.println(deviceId);
            System.out.println(sign);

            if(sign.equalsIgnoreCase("") || timestamp.isEmpty() || deviceId.isEmpty()){
                return Utils.respParamInvalid();
            }
            String collectionId = req.getParameter(PARAM_COLLECTION_ID) != null ? req.getParameter(PARAM_COLLECTION_ID) : "";
            if (collectionId.equalsIgnoreCase("")) {
                return Utils.respParamInvalid();
            }
            param = param + collectionId;
            System.out.println(Utils.genSignKey(deviceId,timestamp,param));
            if(!Utils.genSignKey(deviceId,timestamp,param).equals(sign)){
                return Utils.respSignKeyInvalid();
            }
            List<Map> songList = new ArrayList<>();
            List<SongObject> songRecords = SongModel.getInstance().getSongListByCollectionId(Integer.parseInt(collectionId), page, limit);
            for (SongObject song : songRecords) {
                Map map = new HashMap();
                map.put(PARAM_ID, song.getId());
                map.put(PARAM_NAME, song.getFullname());
                map.put(PARAM_PATH, song.getPath());
                map.put(PARAM_NAME_SINGER, song.getSinger_name());
                map.put(PARAM_THUMBNAIL, song.getThumbnail());
                songList.add(map);
            }
            data.put(PARAM_SONG, songList);
            data.put(PARAM_PATH_SONG, LINK_IMAGE_SONG);
            data.put(PARAM_PATH_COLLECTION, LINK_IMAGE_COLLECTION);
            return Utils.respOK(data);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return Utils.respInternalError();
        }
    }
    public static void main(String[] args) {
        long ts = System.currentTimeMillis();
        String deviceId = "lizks_tv";
        String sign = Utils.genSignKey(deviceId, String.valueOf("1596689608071"), "6");

        System.out.println(ts);
        System.out.println(deviceId);
        System.out.println(sign);

    }
}
