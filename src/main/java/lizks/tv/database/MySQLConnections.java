package lizks.tv.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lizks.tv.util.Config;
import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;

public class MySQLConnections {
    private static final Logger LOGGER = Logger.getLogger(MySQLConnections.class);
    private static final String MYSQL_HOST = Config.getParam("mysql", "host");
    private static final String MYSQL_PORT = Config.getParam("mysql", "port");
    private static final String MYSQL_NAME = Config.getParam("mysql", "lizks/tv/database");
    private static final String MYSQL_USER = Config.getParam("mysql", "user");
    private static final String MYSQL_PASS = Config.getParam("mysql", "password");
    private static final int MYSQL_POOL = Config.getIntParam("mysql", "pool");
    private static final int MYSQL_TIMEOUT = Config.getIntParam("mysql", "timeout");
    private static HikariDataSource ds;
    static {
        try {
            HikariConfig config = new HikariConfig();
            config.setDriverClassName("com.mysql.cj.jdbc.Driver");
            config.setJdbcUrl("jdbc:mysql://" + MYSQL_HOST + ":" + MYSQL_PORT + "/" + MYSQL_NAME);
            config.setUsername(MYSQL_USER);
            config.setPassword(MYSQL_PASS);
            config.setMaximumPoolSize(MYSQL_POOL);
            config.addDataSourceProperty("characterEncoding","utf8");
            config.addDataSourceProperty("useUnicode","true");
            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
            config.setConnectionTimeout(MYSQL_TIMEOUT);
            ds = new HikariDataSource(config);

        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }
//    public static Connection getConnection() {
//        try {
//            Connection conn = ds.getConnection();
//            if (conn != null) {
//                return conn;
//            }
//        } catch (Exception ex) {
//            LOGGER.error(ex.getMessage(), ex);
//        }
//        return null;
//    }

    public static DataSource getDataSource() {
       return ds;
    }

//    public static void safeClose(Connection conn) {
//        if (conn != null) {
//            try {
//                conn.close();
//            } catch (Exception e) {
//                LOGGER.error("safeClose.Connection:" + e.getMessage(), e);
//            }
//        }
//    }

    public static void safeClose(ResultSet res) {
        if (res != null) {
            try {
                res.close();
            } catch (SQLException e) {
                LOGGER.error("safeClose.ResultSet:" + e.getMessage(), e);
            }
        }
    }

    public static void safeClose(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOGGER.error("safeClose.Statement:" + e.getMessage(), e);
            }
        }
    }

    public static void safeClose(PreparedStatement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                LOGGER.error("safeClose.PreparedStatement:" + e.getMessage(), e);
            }
        }
    }
}
