package lizks.tv.database;

import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReadMode;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author hoang
 * created on 8/17/2020
 * inside the package - lizks.tv.database
 */
public class RedissonConnection {

    private static Logger logger = Logger.getLogger(RedissonConnection.class.getName());
    private static final Lock createLock = new ReentrantLock();
    private static RedissonConnection instances = null;
    private RedissonClient redissonClient;

    private static final int port = lizks.tv.util.Config.getIntParam("rest_redis", "port");
    private static final String host = lizks.tv.util.Config.getParam("rest_redis", "host");
    private static final String auth = lizks.tv.util.Config.getParam("rest_redis", "auth");

    public static RedissonConnection getInstance() {
        if (instances == null) {
            createLock.lock();
            try {
                if (instances == null) {
                    instances = new RedissonConnection();
                }
            } finally {
                createLock.unlock();
            }
        }
        return instances;
    }

    private RedissonConnection() {
        Config config = new Config();
        config.useSingleServer()
                .setPassword(StringUtils.isNotEmpty(auth) ? auth : null)
                .setAddress("redis://" + host + ":" + port)
                .setConnectionMinimumIdleSize(5)
                .setConnectionPoolSize(100)
                .setIdleConnectionTimeout(10000)
                .setConnectTimeout(3000)
                .setTimeout(3000);
//                .setRetryAttempts(3)
//                .setRetryInterval(2000)
//                .setReconnectionTimeout(3000)
//                .setFailedAttempts(3);
        this.redissonClient = Redisson.create(config);

    }

    public RedissonClient getClient() {
        return redissonClient;
    }

    public String getString(String key) {
        try {
            RBucket<String> bucket = redissonClient.getBucket(key);
            String result = (String) bucket.get();
            return result;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    public boolean setString(String key, String value, long time, TimeUnit timeUnit) {
        try {
            RBucket<String> bucket = redissonClient.getBucket(key);
            bucket.set(value, time, timeUnit);
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }
    public Object getStrings(String key) {
        try {
            RBucket<Object> bucket = redissonClient.getBucket(key);
            Object result =  bucket.get();
            return result;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return null;
    }

    public boolean setString(String key, Object value, long time, TimeUnit timeUnit) {
        try {
            RBucket<Object> bucket = redissonClient.getBucket(key);
            bucket.set(value, time, timeUnit);
            return true;
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
        return false;
    }
    public boolean isExists(String key) {
        try {
            RBucket<String> bucket = redissonClient.getBucket(key);
            return bucket.isExists();
        } catch (Exception ex) {
            logger.error(ex.getMessage() + " - key: " + key, ex);
        }
        return false;
    }

    public boolean deleteString(String key) {
        try {
            RBucket<String> bucket = redissonClient.getBucket(key);
            return bucket.delete();
        } catch (Exception ex) {
            logger.error(ex.getMessage() + " - key: " + key, ex);
        }
        return false;
    }

    public void stop() throws Exception {
        try {
            redissonClient.shutdown();
            logger.info("Shutdown Redisson OK");
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }
}
