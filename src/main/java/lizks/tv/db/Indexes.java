/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db;


import lizks.tv.db.tables.*;
import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code>hanet_media</code> schema.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index SONG_FULL_NAME = Indexes0.SONG_FULL_NAME;
    public static final Index SONG_ID = Indexes0.SONG_ID;
    public static final Index SONG_ID_2 = Indexes0.SONG_ID_2;
    public static final Index SONG_IS_ACTIVE = Indexes0.SONG_IS_ACTIVE;
    public static final Index SONG_PATH = Indexes0.SONG_PATH;
    public static final Index SONG_UPDATE_AT = Indexes0.SONG_UPDATE_AT;
    public static final Index SONG_COLLECTION_SONG_ID_2 = Indexes0.SONG_COLLECTION_SONG_ID_2;
    public static final Index SONG_COLLECTION_SONG_ID_3 = Indexes0.SONG_COLLECTION_SONG_ID_3;
    public static final Index SONG_FILE_FAMILY_ID = Indexes0.SONG_FILE_FAMILY_ID;
    public static final Index SONG_FILE_H264_FILE_DOWNLOAD = Indexes0.SONG_FILE_H264_FILE_DOWNLOAD;
    public static final Index SONG_FILE_KTV_ID = Indexes0.SONG_FILE_KTV_ID;
    public static final Index SONG_FILE_VIMEO_FILE_DOWNLOAD = Indexes0.SONG_FILE_VIMEO_FILE_DOWNLOAD;
    public static final Index SONG_GENRE_SONG_ID_2 = Indexes0.SONG_GENRE_SONG_ID_2;
    public static final Index SONG_GENRE_SONG_ID_3 = Indexes0.SONG_GENRE_SONG_ID_3;
    public static final Index SONG_LANGUAGE_SONG_ID_2 = Indexes0.SONG_LANGUAGE_SONG_ID_2;
    public static final Index SONG_LANGUAGE_SONG_ID_3 = Indexes0.SONG_LANGUAGE_SONG_ID_3;
    public static final Index SONG_SINGER_SONG_ID_2 = Indexes0.SONG_SINGER_SONG_ID_2;
    public static final Index SONG_SINGER_SONG_ID_3 = Indexes0.SONG_SINGER_SONG_ID_3;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index SONG_FULL_NAME = Internal.createIndex("full_name", Song.SONG, new OrderField[] { Song.SONG.FULL_NAME }, false);
        public static Index SONG_ID = Internal.createIndex("id", Song.SONG, new OrderField[] { Song.SONG.ID }, false);
        public static Index SONG_ID_2 = Internal.createIndex("id_2", Song.SONG, new OrderField[] { Song.SONG.ID, Song.SONG.IS_ACTIVE, Song.SONG.IS_PUBLISH, Song.SONG.UPDATE_AT }, false);
        public static Index SONG_IS_ACTIVE = Internal.createIndex("is_active", Song.SONG, new OrderField[] { Song.SONG.IS_ACTIVE, Song.SONG.CREATE_AT }, false);
        public static Index SONG_PATH = Internal.createIndex("path", Song.SONG, new OrderField[] { Song.SONG.PATH }, false);
        public static Index SONG_UPDATE_AT = Internal.createIndex("update_at", Song.SONG, new OrderField[] { Song.SONG.UPDATE_AT }, false);
        public static Index SONG_COLLECTION_SONG_ID_2 = Internal.createIndex("song_id_2", SongCollection.SONG_COLLECTION, new OrderField[] { SongCollection.SONG_COLLECTION.SONG_ID }, false);
        public static Index SONG_COLLECTION_SONG_ID_3 = Internal.createIndex("song_id_3", SongCollection.SONG_COLLECTION, new OrderField[] { SongCollection.SONG_COLLECTION.SONG_ID, SongCollection.SONG_COLLECTION.COLLECTION_ID }, false);
        public static Index SONG_FILE_FAMILY_ID = Internal.createIndex("id", SongFileFamily.SONG_FILE_FAMILY, new OrderField[] { SongFileFamily.SONG_FILE_FAMILY.ID }, false);
        public static Index SONG_FILE_H264_FILE_DOWNLOAD = Internal.createIndex("file_download", SongFileH264.SONG_FILE_H264, new OrderField[] { SongFileH264.SONG_FILE_H264.FILE_DOWNLOAD }, false);
        public static Index SONG_FILE_KTV_ID = Internal.createIndex("id", SongFileKtv.SONG_FILE_KTV, new OrderField[] { SongFileKtv.SONG_FILE_KTV.ID }, false);
        public static Index SONG_FILE_VIMEO_FILE_DOWNLOAD = Internal.createIndex("file_download", SongFileVimeo.SONG_FILE_VIMEO, new OrderField[] { SongFileVimeo.SONG_FILE_VIMEO.FILE_DOWNLOAD }, false);
        public static Index SONG_GENRE_SONG_ID_2 = Internal.createIndex("song_id_2", SongGenre.SONG_GENRE, new OrderField[] { SongGenre.SONG_GENRE.SONG_ID }, false);
        public static Index SONG_GENRE_SONG_ID_3 = Internal.createIndex("song_id_3", SongGenre.SONG_GENRE, new OrderField[] { SongGenre.SONG_GENRE.SONG_ID, SongGenre.SONG_GENRE.GENRE_ID }, false);
        public static Index SONG_LANGUAGE_SONG_ID_2 = Internal.createIndex("song_id_2", SongLanguage.SONG_LANGUAGE, new OrderField[] { SongLanguage.SONG_LANGUAGE.SONG_ID }, false);
        public static Index SONG_LANGUAGE_SONG_ID_3 = Internal.createIndex("song_id_3", SongLanguage.SONG_LANGUAGE, new OrderField[] { SongLanguage.SONG_LANGUAGE.SONG_ID, SongLanguage.SONG_LANGUAGE.LANGUAGE_ID }, false);
        public static Index SONG_SINGER_SONG_ID_2 = Internal.createIndex("song_id_2", SongSinger.SONG_SINGER, new OrderField[] { SongSinger.SONG_SINGER.SONG_ID }, false);
        public static Index SONG_SINGER_SONG_ID_3 = Internal.createIndex("song_id_3", SongSinger.SONG_SINGER, new OrderField[] { SongSinger.SONG_SINGER.SONG_ID, SongSinger.SONG_SINGER.SINGER_ID }, false);
    }
}
