/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db.tables.records;


import lizks.tv.db.tables.Sessions;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SessionsRecord extends UpdatableRecordImpl<SessionsRecord> implements Record3<String, UInteger, String> {

    private static final long serialVersionUID = 390227993;

    /**
     * Setter for <code>hanet_media.sessions.session_id</code>.
     */
    public void setSessionId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>hanet_media.sessions.session_id</code>.
     */
    public String getSessionId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>hanet_media.sessions.expires</code>.
     */
    public void setExpires(UInteger value) {
        set(1, value);
    }

    /**
     * Getter for <code>hanet_media.sessions.expires</code>.
     */
    public UInteger getExpires() {
        return (UInteger) get(1);
    }

    /**
     * Setter for <code>hanet_media.sessions.data</code>.
     */
    public void setData(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hanet_media.sessions.data</code>.
     */
    public String getData() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, UInteger, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<String, UInteger, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Sessions.SESSIONS.SESSION_ID;
    }

    @Override
    public Field<UInteger> field2() {
        return Sessions.SESSIONS.EXPIRES;
    }

    @Override
    public Field<String> field3() {
        return Sessions.SESSIONS.DATA;
    }

    @Override
    public String component1() {
        return getSessionId();
    }

    @Override
    public UInteger component2() {
        return getExpires();
    }

    @Override
    public String component3() {
        return getData();
    }

    @Override
    public String value1() {
        return getSessionId();
    }

    @Override
    public UInteger value2() {
        return getExpires();
    }

    @Override
    public String value3() {
        return getData();
    }

    @Override
    public SessionsRecord value1(String value) {
        setSessionId(value);
        return this;
    }

    @Override
    public SessionsRecord value2(UInteger value) {
        setExpires(value);
        return this;
    }

    @Override
    public SessionsRecord value3(String value) {
        setData(value);
        return this;
    }

    @Override
    public SessionsRecord values(String value1, UInteger value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached SessionsRecord
     */
    public SessionsRecord() {
        super(Sessions.SESSIONS);
    }

    /**
     * Create a detached, initialised SessionsRecord
     */
    public SessionsRecord(String sessionId, UInteger expires, String data) {
        super(Sessions.SESSIONS);

        set(0, sessionId);
        set(1, expires);
        set(2, data);
    }
}
