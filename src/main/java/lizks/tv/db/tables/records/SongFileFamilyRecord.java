/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db.tables.records;


import lizks.tv.db.tables.SongFileFamily;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SongFileFamilyRecord extends UpdatableRecordImpl<SongFileFamilyRecord> implements Record3<String, String, String> {

    private static final long serialVersionUID = 1287590626;

    /**
     * Setter for <code>hanet_media.song_file_family.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_family.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>hanet_media.song_file_family.file_download</code>.
     */
    public void setFileDownload(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_family.file_download</code>.
     */
    public String getFileDownload() {
        return (String) get(1);
    }

    /**
     * Setter for <code>hanet_media.song_file_family.md5</code>.
     */
    public void setMd5(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_family.md5</code>.
     */
    public String getMd5() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<String, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return SongFileFamily.SONG_FILE_FAMILY.ID;
    }

    @Override
    public Field<String> field2() {
        return SongFileFamily.SONG_FILE_FAMILY.FILE_DOWNLOAD;
    }

    @Override
    public Field<String> field3() {
        return SongFileFamily.SONG_FILE_FAMILY.MD5;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getFileDownload();
    }

    @Override
    public String component3() {
        return getMd5();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getFileDownload();
    }

    @Override
    public String value3() {
        return getMd5();
    }

    @Override
    public SongFileFamilyRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public SongFileFamilyRecord value2(String value) {
        setFileDownload(value);
        return this;
    }

    @Override
    public SongFileFamilyRecord value3(String value) {
        setMd5(value);
        return this;
    }

    @Override
    public SongFileFamilyRecord values(String value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached SongFileFamilyRecord
     */
    public SongFileFamilyRecord() {
        super(SongFileFamily.SONG_FILE_FAMILY);
    }

    /**
     * Create a detached, initialised SongFileFamilyRecord
     */
    public SongFileFamilyRecord(String id, String fileDownload, String md5) {
        super(SongFileFamily.SONG_FILE_FAMILY);

        set(0, id);
        set(1, fileDownload);
        set(2, md5);
    }
}
