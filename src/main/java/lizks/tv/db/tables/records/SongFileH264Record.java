/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db.tables.records;


import lizks.tv.db.tables.SongFileH264;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SongFileH264Record extends UpdatableRecordImpl<SongFileH264Record> implements Record3<String, String, String> {

    private static final long serialVersionUID = 1498143386;

    /**
     * Setter for <code>hanet_media.song_file_h264.file_download</code>.
     */
    public void setFileDownload(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_h264.file_download</code>.
     */
    public String getFileDownload() {
        return (String) get(0);
    }

    /**
     * Setter for <code>hanet_media.song_file_h264.md5</code>.
     */
    public void setMd5(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_h264.md5</code>.
     */
    public String getMd5() {
        return (String) get(1);
    }

    /**
     * Setter for <code>hanet_media.song_file_h264.id</code>.
     */
    public void setId(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_h264.id</code>.
     */
    public String getId() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row3<String, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<String, String, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return SongFileH264.SONG_FILE_H264.FILE_DOWNLOAD;
    }

    @Override
    public Field<String> field2() {
        return SongFileH264.SONG_FILE_H264.MD5;
    }

    @Override
    public Field<String> field3() {
        return SongFileH264.SONG_FILE_H264.ID;
    }

    @Override
    public String component1() {
        return getFileDownload();
    }

    @Override
    public String component2() {
        return getMd5();
    }

    @Override
    public String component3() {
        return getId();
    }

    @Override
    public String value1() {
        return getFileDownload();
    }

    @Override
    public String value2() {
        return getMd5();
    }

    @Override
    public String value3() {
        return getId();
    }

    @Override
    public SongFileH264Record value1(String value) {
        setFileDownload(value);
        return this;
    }

    @Override
    public SongFileH264Record value2(String value) {
        setMd5(value);
        return this;
    }

    @Override
    public SongFileH264Record value3(String value) {
        setId(value);
        return this;
    }

    @Override
    public SongFileH264Record values(String value1, String value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached SongFileH264Record
     */
    public SongFileH264Record() {
        super(SongFileH264.SONG_FILE_H264);
    }

    /**
     * Create a detached, initialised SongFileH264Record
     */
    public SongFileH264Record(String fileDownload, String md5, String id) {
        super(SongFileH264.SONG_FILE_H264);

        set(0, fileDownload);
        set(1, md5);
        set(2, id);
    }
}
