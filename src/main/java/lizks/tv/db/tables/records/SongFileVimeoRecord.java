/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db.tables.records;


import lizks.tv.db.tables.SongFileVimeo;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class SongFileVimeoRecord extends UpdatableRecordImpl<SongFileVimeoRecord> implements Record6<String, String, String, String, String, Integer> {

    private static final long serialVersionUID = -1426212024;

    /**
     * Setter for <code>hanet_media.song_file_vimeo.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>hanet_media.song_file_vimeo.file_download</code>.
     */
    public void setFileDownload(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.file_download</code>.
     */
    public String getFileDownload() {
        return (String) get(1);
    }

    /**
     * Setter for <code>hanet_media.song_file_vimeo.vimeoId</code>.
     */
    public void setVimeoid(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.vimeoId</code>.
     */
    public String getVimeoid() {
        return (String) get(2);
    }

    /**
     * Setter for <code>hanet_media.song_file_vimeo.url</code>.
     */
    public void setUrl(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.url</code>.
     */
    public String getUrl() {
        return (String) get(3);
    }

    /**
     * Setter for <code>hanet_media.song_file_vimeo.url_mp4</code>.
     */
    public void setUrlMp4(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.url_mp4</code>.
     */
    public String getUrlMp4() {
        return (String) get(4);
    }

    /**
     * Setter for <code>hanet_media.song_file_vimeo.create_at</code>.
     */
    public void setCreateAt(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>hanet_media.song_file_vimeo.create_at</code>.
     */
    public Integer getCreateAt() {
        return (Integer) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row6<String, String, String, String, String, Integer> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    @Override
    public Row6<String, String, String, String, String, Integer> valuesRow() {
        return (Row6) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return SongFileVimeo.SONG_FILE_VIMEO.ID;
    }

    @Override
    public Field<String> field2() {
        return SongFileVimeo.SONG_FILE_VIMEO.FILE_DOWNLOAD;
    }

    @Override
    public Field<String> field3() {
        return SongFileVimeo.SONG_FILE_VIMEO.VIMEOID;
    }

    @Override
    public Field<String> field4() {
        return SongFileVimeo.SONG_FILE_VIMEO.URL;
    }

    @Override
    public Field<String> field5() {
        return SongFileVimeo.SONG_FILE_VIMEO.URL_MP4;
    }

    @Override
    public Field<Integer> field6() {
        return SongFileVimeo.SONG_FILE_VIMEO.CREATE_AT;
    }

    @Override
    public String component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getFileDownload();
    }

    @Override
    public String component3() {
        return getVimeoid();
    }

    @Override
    public String component4() {
        return getUrl();
    }

    @Override
    public String component5() {
        return getUrlMp4();
    }

    @Override
    public Integer component6() {
        return getCreateAt();
    }

    @Override
    public String value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getFileDownload();
    }

    @Override
    public String value3() {
        return getVimeoid();
    }

    @Override
    public String value4() {
        return getUrl();
    }

    @Override
    public String value5() {
        return getUrlMp4();
    }

    @Override
    public Integer value6() {
        return getCreateAt();
    }

    @Override
    public SongFileVimeoRecord value1(String value) {
        setId(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord value2(String value) {
        setFileDownload(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord value3(String value) {
        setVimeoid(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord value4(String value) {
        setUrl(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord value5(String value) {
        setUrlMp4(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord value6(Integer value) {
        setCreateAt(value);
        return this;
    }

    @Override
    public SongFileVimeoRecord values(String value1, String value2, String value3, String value4, String value5, Integer value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached SongFileVimeoRecord
     */
    public SongFileVimeoRecord() {
        super(SongFileVimeo.SONG_FILE_VIMEO);
    }

    /**
     * Create a detached, initialised SongFileVimeoRecord
     */
    public SongFileVimeoRecord(String id, String fileDownload, String vimeoid, String url, String urlMp4, Integer createAt) {
        super(SongFileVimeo.SONG_FILE_VIMEO);

        set(0, id);
        set(1, fileDownload);
        set(2, vimeoid);
        set(3, url);
        set(4, urlMp4);
        set(5, createAt);
    }
}
