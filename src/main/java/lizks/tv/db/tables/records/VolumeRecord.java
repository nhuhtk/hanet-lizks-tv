/*
 * This file is generated by jOOQ.
 */
package lizks.tv.db.tables.records;


import lizks.tv.db.tables.Volume;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Row10;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class VolumeRecord extends UpdatableRecordImpl<VolumeRecord> implements Record10<UInteger, String, String, String, Integer, String, String, Integer, Integer, Integer> {

    private static final long serialVersionUID = 865506397;

    /**
     * Setter for <code>hanet_media.volume.id</code>.
     */
    public void setId(UInteger value) {
        set(0, value);
    }

    /**
     * Getter for <code>hanet_media.volume.id</code>.
     */
    public UInteger getId() {
        return (UInteger) get(0);
    }

    /**
     * Setter for <code>hanet_media.volume.name</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>hanet_media.volume.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>hanet_media.volume.content</code>.
     */
    public void setContent(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hanet_media.volume.content</code>.
     */
    public String getContent() {
        return (String) get(2);
    }

    /**
     * Setter for <code>hanet_media.volume.description</code>.
     */
    public void setDescription(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>hanet_media.volume.description</code>.
     */
    public String getDescription() {
        return (String) get(3);
    }

    /**
     * Setter for <code>hanet_media.volume.status</code>.
     */
    public void setStatus(Integer value) {
        set(4, value);
    }

    /**
     * Getter for <code>hanet_media.volume.status</code>.
     */
    public Integer getStatus() {
        return (Integer) get(4);
    }

    /**
     * Setter for <code>hanet_media.volume.create_by</code>.
     */
    public void setCreateBy(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>hanet_media.volume.create_by</code>.
     */
    public String getCreateBy() {
        return (String) get(5);
    }

    /**
     * Setter for <code>hanet_media.volume.update_by</code>.
     */
    public void setUpdateBy(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>hanet_media.volume.update_by</code>.
     */
    public String getUpdateBy() {
        return (String) get(6);
    }

    /**
     * Setter for <code>hanet_media.volume.create_at</code>.
     */
    public void setCreateAt(Integer value) {
        set(7, value);
    }

    /**
     * Getter for <code>hanet_media.volume.create_at</code>.
     */
    public Integer getCreateAt() {
        return (Integer) get(7);
    }

    /**
     * Setter for <code>hanet_media.volume.update_at</code>.
     */
    public void setUpdateAt(Integer value) {
        set(8, value);
    }

    /**
     * Getter for <code>hanet_media.volume.update_at</code>.
     */
    public Integer getUpdateAt() {
        return (Integer) get(8);
    }

    /**
     * Setter for <code>hanet_media.volume.publishAt</code>.
     */
    public void setPublishat(Integer value) {
        set(9, value);
    }

    /**
     * Getter for <code>hanet_media.volume.publishAt</code>.
     */
    public Integer getPublishat() {
        return (Integer) get(9);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<UInteger> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record10 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row10<UInteger, String, String, String, Integer, String, String, Integer, Integer, Integer> fieldsRow() {
        return (Row10) super.fieldsRow();
    }

    @Override
    public Row10<UInteger, String, String, String, Integer, String, String, Integer, Integer, Integer> valuesRow() {
        return (Row10) super.valuesRow();
    }

    @Override
    public Field<UInteger> field1() {
        return Volume.VOLUME.ID;
    }

    @Override
    public Field<String> field2() {
        return Volume.VOLUME.NAME;
    }

    @Override
    public Field<String> field3() {
        return Volume.VOLUME.CONTENT;
    }

    @Override
    public Field<String> field4() {
        return Volume.VOLUME.DESCRIPTION;
    }

    @Override
    public Field<Integer> field5() {
        return Volume.VOLUME.STATUS;
    }

    @Override
    public Field<String> field6() {
        return Volume.VOLUME.CREATE_BY;
    }

    @Override
    public Field<String> field7() {
        return Volume.VOLUME.UPDATE_BY;
    }

    @Override
    public Field<Integer> field8() {
        return Volume.VOLUME.CREATE_AT;
    }

    @Override
    public Field<Integer> field9() {
        return Volume.VOLUME.UPDATE_AT;
    }

    @Override
    public Field<Integer> field10() {
        return Volume.VOLUME.PUBLISHAT;
    }

    @Override
    public UInteger component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getName();
    }

    @Override
    public String component3() {
        return getContent();
    }

    @Override
    public String component4() {
        return getDescription();
    }

    @Override
    public Integer component5() {
        return getStatus();
    }

    @Override
    public String component6() {
        return getCreateBy();
    }

    @Override
    public String component7() {
        return getUpdateBy();
    }

    @Override
    public Integer component8() {
        return getCreateAt();
    }

    @Override
    public Integer component9() {
        return getUpdateAt();
    }

    @Override
    public Integer component10() {
        return getPublishat();
    }

    @Override
    public UInteger value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getName();
    }

    @Override
    public String value3() {
        return getContent();
    }

    @Override
    public String value4() {
        return getDescription();
    }

    @Override
    public Integer value5() {
        return getStatus();
    }

    @Override
    public String value6() {
        return getCreateBy();
    }

    @Override
    public String value7() {
        return getUpdateBy();
    }

    @Override
    public Integer value8() {
        return getCreateAt();
    }

    @Override
    public Integer value9() {
        return getUpdateAt();
    }

    @Override
    public Integer value10() {
        return getPublishat();
    }

    @Override
    public VolumeRecord value1(UInteger value) {
        setId(value);
        return this;
    }

    @Override
    public VolumeRecord value2(String value) {
        setName(value);
        return this;
    }

    @Override
    public VolumeRecord value3(String value) {
        setContent(value);
        return this;
    }

    @Override
    public VolumeRecord value4(String value) {
        setDescription(value);
        return this;
    }

    @Override
    public VolumeRecord value5(Integer value) {
        setStatus(value);
        return this;
    }

    @Override
    public VolumeRecord value6(String value) {
        setCreateBy(value);
        return this;
    }

    @Override
    public VolumeRecord value7(String value) {
        setUpdateBy(value);
        return this;
    }

    @Override
    public VolumeRecord value8(Integer value) {
        setCreateAt(value);
        return this;
    }

    @Override
    public VolumeRecord value9(Integer value) {
        setUpdateAt(value);
        return this;
    }

    @Override
    public VolumeRecord value10(Integer value) {
        setPublishat(value);
        return this;
    }

    @Override
    public VolumeRecord values(UInteger value1, String value2, String value3, String value4, Integer value5, String value6, String value7, Integer value8, Integer value9, Integer value10) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached VolumeRecord
     */
    public VolumeRecord() {
        super(Volume.VOLUME);
    }

    /**
     * Create a detached, initialised VolumeRecord
     */
    public VolumeRecord(UInteger id, String name, String content, String description, Integer status, String createBy, String updateBy, Integer createAt, Integer updateAt, Integer publishat) {
        super(Volume.VOLUME);

        set(0, id);
        set(1, name);
        set(2, content);
        set(3, description);
        set(4, status);
        set(5, createBy);
        set(6, updateBy);
        set(7, createAt);
        set(8, updateAt);
        set(9, publishat);
    }
}
