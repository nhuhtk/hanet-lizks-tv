package lizks.tv.model;

import lizks.tv.database.MySQLConnections;
import lizks.tv.database.RedissonConnection;
import lizks.tv.db.Tables;
import lizks.tv.db.tables.records.SettingRecord;
import lizks.tv.object.CollectionObject;
import org.apache.log4j.Logger;
import org.jooq.*;
import org.jooq.impl.DSL;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static lizks.tv.util.Constant.*;

/**
 * @author hoang
 * created on 8/19/2020
 * inside the package - lizks.tv.model
 */
public class CollectionModel {
    private static final Logger LOGGER = Logger.getLogger(CollectionModel.class);
    private static final Lock CREATE_LOCK = new ReentrantLock();
    private static CollectionModel instance = null;

    public static CollectionModel getInstance() {
        if (instance == null) {
            CREATE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new CollectionModel();
                }
            } catch (Exception ex) {
            } finally {
                CREATE_LOCK.unlock();
            }
        }
        return instance;
    }
    private String getListIDHotCollection(){
        String idSongs = "";
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SettingRecord> query = context.selectFrom(Tables.SETTING).where(Tables.SETTING.OPTION_KEY.eq(SETTING_HOT_COLLECTION));
            SettingRecord settingRecord = query.fetchOne();
            idSongs = settingRecord.getOptionValue();
            LOGGER.info("getListIDHotCollection "+idSongs);
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return idSongs;
    }
    private List<CollectionObject> getHotCollectionsByCache(){
        List<CollectionObject> collectionList = (List<CollectionObject>) RedissonConnection.getInstance().getStrings(REDIS_KEY_LIST_HOT_COLLECTION);
        LOGGER.info("get hot collection from caching: ");
        return collectionList;
    }
    public List<CollectionObject> getHotCollections(){
        List<CollectionObject> collectionList = new ArrayList<>();
        try {
            String idCollections = getListIDHotCollection();
            LOGGER.info("getListIdCollection ids: "+idCollections);
            collectionList = getHotCollectionsByCache();
            if(collectionList == null || collectionList.size() == 0){
                collectionList = getCollectionListByIds(idCollections);
                RedissonConnection.getInstance().setString(REDIS_KEY_LIST_HOT_COLLECTION, collectionList, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                LOGGER.info("get hot collection from database: " + collectionList.size());
            }

        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return collectionList;
    }
    private  List<CollectionObject> getCollectionListByIds(String ids){
        List<CollectionObject> songs = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<Record> query = context.select().from(Tables.COLLECTION)
                    .where(Tables.COLLECTION.ID.in(Arrays.asList(ids.split(PREFIX_REGIX))));
            Result<Record> records = query.fetch();
            for(Record record :records){
                CollectionObject collection = new CollectionObject();
                collection.setId(record.get(Tables.COLLECTION.ID));
                collection.setName(record.get(Tables.COLLECTION.NAME));
                collection.setThumbnail(String.valueOf(record.get(Tables.COLLECTION.ID)).concat(PREFIX_JPEG));
                songs.add(collection);
            }

            LOGGER.info("getSongById name: "+songs.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }
}
