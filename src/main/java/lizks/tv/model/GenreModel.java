package lizks.tv.model;

import lizks.tv.database.MySQLConnections;
import lizks.tv.database.RedissonConnection;
import lizks.tv.db.Tables;
import lizks.tv.db.tables.records.SettingRecord;
import lizks.tv.object.GenreObject;
import org.apache.log4j.Logger;
import org.jooq.*;
import org.jooq.impl.DSL;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static lizks.tv.util.Constant.*;


/**
 * @author hoang
 * created on 8/5/2020
 * inside the package - lizks.tv.model
 */
public class GenreModel {
    private static final Logger LOGGER = Logger.getLogger(SongModel.class);
    private static final Lock CREATE_LOCK = new ReentrantLock();
    private static GenreModel instance = null;

    public static GenreModel getInstance() {
        if (instance == null) {
            CREATE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new GenreModel();
                }
            } catch (Exception ex) {
            } finally {
                CREATE_LOCK.unlock();
            }
        }
        return instance;
    }
    public List<GenreObject> getHotGenres(){
        List<GenreObject> genreList = new ArrayList<>();
        try {
            String idGenres = getListIDHotGenre();
            LOGGER.info("getHotGenres ids: "+idGenres);
            genreList = getHotGenresByCache();
            if(genreList == null || genreList.size() == 0) {
                genreList = getGenreListByIds(idGenres);
                RedissonConnection.getInstance().setString(REDIS_KEY_LIST_HOT_GENRE, genreList, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                LOGGER.info("getHotGenres by database : ");
            }

        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return genreList;
    }
    public List<GenreObject> getHotGenresByCache() {
        List<GenreObject> genreList = ( List<GenreObject>)RedissonConnection.getInstance().getStrings(REDIS_KEY_LIST_HOT_GENRE);
        LOGGER.info("getHotGenresByCache : ");
        return genreList;
    }
    private String getListIDHotGenre(){
        String idGenres = "";
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SettingRecord> query = context.selectFrom(Tables.SETTING).where(Tables.SETTING.OPTION_KEY.eq(SETTING_HOT_GENRE));
            SettingRecord settingRecord = query.fetchOne();
            idGenres = settingRecord.getOptionValue();
            LOGGER.info("getListIDHotGenre "+idGenres);
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return idGenres;
    }
    private  List<GenreObject> getGenreListByIds(String ids){
        List<GenreObject> genres = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<Record> query = context.select().from(Tables.GENRE)
                    .where(Tables.GENRE.ID.in(Arrays.asList(ids.split(PREFIX_REGIX))));
            Result<Record> records = query.fetch();
            for(Record record :records){
                GenreObject genre = new GenreObject();
                genre.setId(String.valueOf(record.get(Tables.GENRE.ID)));
                genre.setName(record.get(Tables.GENRE.NAME));
                genre.setThumbnail(String.valueOf(record.get(Tables.GENRE.ID)).concat(PREFIX_JPEG));
                genres.add(genre);
            }
            LOGGER.info("getGenreListByIds : "+genres.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return genres;
    }
}
