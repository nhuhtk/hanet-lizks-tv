package lizks.tv.model;

import lizks.tv.database.MySQLConnections;
import lizks.tv.database.RedissonConnection;
import lizks.tv.db.Tables;
import lizks.tv.db.tables.records.SettingRecord;
import lizks.tv.db.tables.records.SingerRecord;
import lizks.tv.object.SingerObject;
import lizks.tv.util.VNCharacterUtils;
import org.apache.log4j.Logger;
import org.jooq.*;
import org.jooq.impl.DSL;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static lizks.tv.util.Constant.*;

/**
 * @author hoang
 * created on 8/5/2020
 * inside the package - lizks.tv.model
 */
public class SingerModel {
    private static final Logger LOGGER = Logger.getLogger(SongModel.class);
    private static final Lock CREATE_LOCK = new ReentrantLock();
    private static SingerModel instance = null;

    public static SingerModel getInstance() {
        if (instance == null) {
            CREATE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new SingerModel();
                }
            } catch (Exception ex) {
            } finally {
                CREATE_LOCK.unlock();
            }
        }
        return instance;
    }
    public List<SingerObject> getHotSingers(){
        List<SingerObject> singerList = new ArrayList<>();
        try {
            String idSongs = getListIDHotSinger();
            LOGGER.info("getHotSingers ids: "+idSongs);
            singerList = getHotSingersByCache();
            if(singerList == null || singerList.size() ==0) {
                singerList = getSingerListByIds(idSongs);
                RedissonConnection.getInstance().setString(REDIS_KEY_LIST_HOT_SINGER, singerList, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                LOGGER.info("getHotSingers by database singerList: " + singerList.size());
            }
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return singerList;
    }
    public List<SingerObject> getHotSingersByCache(){
        List<SingerObject> singerList = (List<SingerObject>) RedissonConnection.getInstance().getStrings(REDIS_KEY_LIST_HOT_SINGER);
        LOGGER.info("getHotSingersByCache singerList: ");
        return singerList;
    }
    private String getListIDHotSinger(){
        String idSingers = "";
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SettingRecord> query = context.selectFrom(Tables.SETTING).where(Tables.SETTING.OPTION_KEY.eq(SETTING_HOT_SINGER));
            SettingRecord settingRecord = query.fetchOne();
            idSingers = settingRecord.getOptionValue();
            LOGGER.info("getListIDHotSinger "+idSingers);
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return idSingers;
    }
    private  List<SingerObject> getSingerListByIds(String ids){
        List<SingerObject> singers = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<Record> query = context.select().from(Tables.SINGER)
                    .where(Tables.SINGER.ID.in(Arrays.asList(ids.split(PREFIX_REGIX))));
            Result<Record> records = query.fetch();
            for(Record record :records){
                SingerObject singer = new SingerObject();
                singer.setId(record.get(Tables.SINGER.ID));
                singer.setFullname(record.get(Tables.SINGER.FULL_NAME));
                singer.setPath(String.valueOf(record.get(Tables.SINGER.ID)).concat(PREFIX_JPEG));
                singers.add(singer);
            }
            LOGGER.info("getSingerListByIds : "+singers.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return singers;
    }
    public SingerRecord getSingById(int id){
        SingerRecord singer = null;
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SingerRecord> query = context.selectFrom(Tables.SINGER).where(Tables.SINGER.ID.equal(id));
            singer = query.fetchOne();
            LOGGER.info("getSingById name: "+singer.getFullName());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return singer;
    }
    public List<Object> querySearchSinger(String search){
        List<Object> songs = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            String text = VNCharacterUtils.removeAccent(search);
//            String keyword = "%"+text+"%";
            String keyword = text;
            String strSearch =  VNCharacterUtils.removeSpace(keyword) ;
//            String shortKeyword = "%"+VNCharacterUtils.getFirstCharacter(text)+"%";
//            shortKeyword = "%"+shortKeyword+"%";
            LOGGER.info("keyword : "+keyword);
//            LOGGER.info("str : "+strSearch);
//            LOGGER.info("shortKeyword : "+shortKeyword);
            SelectLimitPercentStep<Record> query = context.select().from(Tables.SINGER)
                    .where(Tables.SINGER.FULL_NAME.like(keyword))
                    .or(Tables.SINGER.NAME_OF_SEARCH.like(strSearch)).limit(5);
//                    .or(Tables.SINGER.SHORT_NAME.like(shortKeyword));
            Result<Record> records = query.fetch();
            for(Record record :records){
                SingerObject singer = new SingerObject();
                singer.setId(record.get(Tables.SINGER.ID));
                singer.setFullname(record.get(Tables.SINGER.FULL_NAME));
                singer.setPath(String.valueOf(record.get(Tables.SINGER.ID)).concat(PREFIX_JPEG));
                songs.add(singer);
            }
            LOGGER.info("querySearchSinger : "+songs.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }
}
