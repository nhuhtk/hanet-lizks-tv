package lizks.tv.model;

import lizks.tv.database.MySQLConnections;
import lizks.tv.database.RedissonConnection;
import lizks.tv.db.Tables;
import lizks.tv.db.tables.records.SettingRecord;
import lizks.tv.db.tables.records.SingerRecord;
import lizks.tv.db.tables.records.SongRecord;
import lizks.tv.object.SongObject;
import lizks.tv.util.VNCharacterUtils;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Table;
import org.jooq.*;
import org.jooq.impl.DSL;
import org.jooq.util.mysql.MySQLDataType;
import org.omg.IOP.TAG_ALTERNATE_IIOP_ADDRESS;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static lizks.tv.util.Constant.*;

/**
 * @author hoang
 * created on 8/4/2020
 * inside the package - lizks.tv.model
 */
public class SongModel {
    private static final Logger LOGGER = Logger.getLogger(SongModel.class);
    private static final Lock CREATE_LOCK = new ReentrantLock();
    private static SongModel instance = null;

    public static SongModel getInstance() {
        if (instance == null) {
            CREATE_LOCK.lock();
            try {
                if (instance == null) {
                    instance = new SongModel();
                }
            } catch (Exception ex) {
            } finally {
                CREATE_LOCK.unlock();
            }
        }
        return instance;
    }
    public List<SongObject> getHotSongs(){
        List<SongObject> songList = new ArrayList<>();
        try {
            String idSongs = getListIDHotSong();
            LOGGER.info("getListSongByLastupdate ids: "+idSongs);
            songList = getHotSongsByCache();
            if(songList == null || songList.size() == 0){
                songList = getSongListByIds(idSongs);
                RedissonConnection.getInstance().setString(REDIS_KEY_LIST_HOT_SONG, songList, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                LOGGER.info("get hot song from database: " + songList.size());
            }

        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songList;
    }

    private String getListIDHotSong(){
        String idSongs = "";
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SettingRecord> query = context.selectFrom(Tables.SETTING).where(Tables.SETTING.OPTION_KEY.eq(SETTING_HOT_SONG));
            SettingRecord settingRecord = query.fetchOne();
            idSongs = settingRecord.getOptionValue();
            LOGGER.info("getIDHotSongs "+idSongs);
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return idSongs;
    }
    private List<SongObject> getHotSongsByCache(){
        List<SongObject> songList = (List<SongObject>) RedissonConnection.getInstance().getStrings(REDIS_KEY_LIST_HOT_SONG);
        LOGGER.info("get hot song from caching: ");
        return songList;
    }
    private SongRecord getSongById(String id){
        SongRecord song = null;
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<SongRecord> query = context.selectFrom(Tables.SONG)
                    .where(Tables.SONG.ID.eq(id))
            .and(DSL.cast(Tables.SONG.IS_ACTIVE, MySQLDataType.BINARY).eq(DSL.cast("1",MySQLDataType.BINARY)));
            song = query.fetchOne();
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return song;
    }
    private  List<SongObject> getSongListByIds(String ids){
        List<SongObject> songs = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectConditionStep<Record> query = context.select().from(Tables.SONG)
                    .join(Tables.SONG_SINGER)
                    .on(Tables.SONG_SINGER.SONG_ID.eq(Tables.SONG.ID))
                    .join(Tables.SINGER).on(Tables.SINGER.ID.eq(Tables.SONG_SINGER.SINGER_ID))
                    .where(Tables.SONG.ID.in(ids.split(PREFIX_REGIX)))
                    .and(DSL.cast(Tables.SONG.IS_ACTIVE, MySQLDataType.BINARY).eq(DSL.cast("1",MySQLDataType.BINARY)));
            Result<Record> records = query.fetch();
            for(Record record :records){
                SongObject song = new SongObject();
                song.setId(record.get(Tables.SONG.ID));
                song.setFullname(record.get(Tables.SONG.FULL_NAME));
                song.setPath(record.get(Tables.SONG.PATH));
                song.setThumbnail(song.getPath().replace(PREFIX_MP4,PREFIX_JPEG));
                song.setSinger_name(record.get(Tables.SINGER.FULL_NAME));
                song.setSinger_id(record.get(Tables.SINGER.ID));
                songs.add(song);
            }

            LOGGER.info("getSongById name: "+songs.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }
    public SongObject getSongByIdFromCache(String songId){

        SongObject song = (SongObject) RedissonConnection.getInstance().getStrings(REDIS_KEY_GET_SONG_BY_ID + songId);
        return song;
    }
    public   List<SongObject> getSongListBySingerId(int singerId, int page, int number){
        List<SongObject> songs = new ArrayList<>();
        try {
            DataSource dataSource = MySQLConnections.getDataSource();
            int offset = (page -1) *number;
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectLimitPercentAfterOffsetStep<Record> query = context.select().from(Tables.SONG_SINGER)
                    .join(Tables.SONG).on(Tables.SONG_SINGER.SONG_ID.eq(Tables.SONG.ID))
                    .where(Tables.SONG_SINGER.SINGER_ID.eq(singerId)).orderBy(Tables.SONG.CREATE_AT.desc()).offset(offset).limit(number);
            Result<Record> records = query.fetch();
            for(Record record :records){
                SongObject songCache = getSongByIdFromCache(record.get(Tables.SONG_SINGER.SONG_ID));
                SingerRecord singerRecord = SingerModel.getInstance().getSingById(record.get(Tables.SONG_SINGER.SINGER_ID));
                if(songCache == null) {
                    SongRecord songRecord = getSongById(record.get(Tables.SONG_SINGER.SONG_ID));
                    SongObject song = new SongObject();
                    song.setId(songRecord.getId());
                    song.setFullname(songRecord.getFullName());
                    song.setPath(songRecord.getPath());
                    song.setThumbnail(song.getPath().replace(PREFIX_MP4, PREFIX_JPEG));
                    song.setSinger_name(singerRecord.getFullName());
                    song.setSinger_id(singerRecord.getId());
                    songs.add(song);
                    RedissonConnection.getInstance().setString(REDIS_KEY_GET_SONG_BY_ID + song.getId(),song, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                    LOGGER.error("getsongbydatabase : "+REDIS_KEY_GET_SONG_BY_ID + song.getId());
                }else {
                    songs.add(songCache);
                    LOGGER.error("getsongbycache : "+REDIS_KEY_GET_SONG_BY_ID + songCache.getId());
                }
            }
            LOGGER.info("getSongListBySingerId : "+songs.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }

    public List<SongObject> getSongListByGenreId(int genreId,int page, int number){
        List<SongObject> songs = new ArrayList<>();
        try {
            int offset = (page -1) *number;
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectLimitPercentAfterOffsetStep<Record> query = context.select().from(Tables.SONG_GENRE)
                    .where(Tables.SONG_GENRE.GENRE_ID.eq(genreId)).offset(offset).limit(number);;
            Result<Record> records = query.fetch();
            for(Record record :records){
                String songId = record.get(Tables.SONG_GENRE.SONG_ID);
                SongObject songCache = getSongByIdFromCache(songId);
                if(songCache == null) {
                    SongRecord songRecord = getSongById(record.get(Tables.SONG_SINGER.SONG_ID));
                    SongObject song = new SongObject();
                    if(songRecord == null){
                        return songs;
                    }
                    song.setId(songRecord.getId());
                    song.setFullname(songRecord.getFullName());
                    song.setPath(songRecord.getPath());
                    song.setThumbnail(song.getPath().replace(PREFIX_MP4, PREFIX_JPEG));
                    song.setSinger_name(song.getSinger_name());
                    song.setSinger_id(song.getSinger_id());
                    songs.add(song);
                    RedissonConnection.getInstance().setString(REDIS_KEY_GET_SONG_BY_ID + song.getId(),song, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                    LOGGER.error("getsongbydatabase : "+REDIS_KEY_GET_SONG_BY_ID + song.getId());
                }else {
                    songs.add(songCache);
                    LOGGER.error("getsongbycache : "+REDIS_KEY_GET_SONG_BY_ID + songCache.getId());
                }
            }
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }

    public List<SongObject> getSongListByCollectionId(int collectionId,int page, int number){
        List<SongObject> songs = new ArrayList<>();
        try {
            int offset = (page -1) *number;
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            SelectLimitPercentAfterOffsetStep<Record> query = context.select().from(Tables.SONG_COLLECTION)
                    .where(Tables.SONG_COLLECTION.COLLECTION_ID.eq(collectionId)).offset(offset).limit(number);;
            Result<Record> records = query.fetch();
            for(Record record :records){
                String songId = String.valueOf(record.get(Tables.SONG_COLLECTION.SONG_ID));
                SongObject songCache = getSongByIdFromCache(songId);
                if(songCache == null) {
                    SongRecord songRecord = getSongById(songId);
                    SongObject song = new SongObject();
                    song.setId(songRecord.getId());
                    song.setFullname(songRecord.getFullName());
                    song.setPath(songRecord.getPath());
                    song.setThumbnail(song.getPath().replace(PREFIX_MP4, PREFIX_JPEG));
                    song.setSinger_name(song.getSinger_name());
                    song.setSinger_id(song.getSinger_id());
                    songs.add(song);
                    RedissonConnection.getInstance().setString(REDIS_KEY_GET_SONG_BY_ID + song.getId(),song, REDIS_EXPIRE_TIME_IN_DAY, TimeUnit.DAYS);
                    LOGGER.error("getsongbydatabase : "+REDIS_KEY_GET_SONG_BY_ID + song.getId());
                }else {
                    songs.add(songCache);
                    LOGGER.error("getsongbycache : "+REDIS_KEY_GET_SONG_BY_ID + songCache.getId());
                }
            }
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
        }
        return songs;
    }
    public List<Object> querySearchSong(String search, int page, int limit){
        List<Object> songs = new ArrayList<>();
        try {
            int offset = (page -1) *limit;
            DataSource dataSource = MySQLConnections.getDataSource();
            DSLContext context = DSL.using(dataSource, SQLDialect.MYSQL);
            String text = VNCharacterUtils.removeAccent(search);
            String keyword = "%"+text+"%";
            String strSearch =  VNCharacterUtils.removeSpace(keyword) ;
            String shortKeyword = VNCharacterUtils.getFirstCharacter(text)+"%";
            LOGGER.info("keyword : "+keyword);
            LOGGER.info("str : "+strSearch);
            LOGGER.info("shortKeyword : "+shortKeyword);
            SelectLimitPercentAfterOffsetStep<Record> query = context.select().from(Tables.SONG)
                    .join(Tables.SONG_SINGER)
                    .on(Tables.SONG_SINGER.SONG_ID.eq(Tables.SONG.ID))
                    .join(Tables.SINGER).on(Tables.SINGER.ID.eq(Tables.SONG_SINGER.SINGER_ID))
                    .where(Tables.SONG.NAME_OF_SEARCH.like(strSearch))
                    .or(Tables.SONG.FULL_NAME.like(keyword))
                    .and(DSL.cast(Tables.SONG.IS_ACTIVE, MySQLDataType.BINARY).eq(DSL.cast("1",MySQLDataType.BINARY)))
                    .orderBy(Tables.SONG.CREATE_AT.desc()).offset(offset).limit(limit);
            LOGGER.info("query : "+query);
            Record[] records = query.fetchArray();
            for(Record record :records){
                SongObject song = new SongObject();
                song.setId(record.get(Tables.SONG.ID));
                song.setFullname(record.get(Tables.SONG.FULL_NAME));
                song.setPath(record.get(Tables.SONG.PATH));
                song.setThumbnail(song.getPath().replace(PREFIX_MP4,PREFIX_JPEG));
                song.setSinger_name(record.get(Tables.SINGER.FULL_NAME));
                if(song.getSinger_name().equals("unknown"))
                    song.setSinger_name("");
                songs.add(song);
            }
            LOGGER.info("querySearchSong : "+songs.size());
        } catch (Exception exception){
            LOGGER.error(exception.getMessage(), exception);
            return songs;
        }
        return songs;
    }

    public List<Object> querySearch(String query, int page, int limit) {
        List<Object> songs = new ArrayList<>();
        if(page == 1 && query.length() > 1) {
            songs.addAll(SingerModel.getInstance().querySearchSinger(query));
        }
        songs.addAll(querySearchSong(query,page,limit));
        return songs;
    }


}
