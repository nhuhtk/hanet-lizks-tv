package lizks.tv.object;

import java.io.Serializable;

/**
 * @author hoang
 * created on 8/4/2020
 * inside the package - Object
 */
public class SingerObject implements Serializable {

    private int id;
    private String fullname;
    private String name_of_search;
    private String short_name;
    private String bonus_data;
    private int is_active;
    private int description;
    private String create_by;
    private String create_at;
    private String update_at;
    private String update_by;
    private String path;


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getName_of_search() {
        return name_of_search;
    }

    public void setName_of_search(String name_of_search) {
        this.name_of_search = name_of_search;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }

    public String getBonus_data() {
        return bonus_data;
    }

    public void setBonus_data(String bonus_data) {
        this.bonus_data = bonus_data;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getUpdate_by() {
        return update_by;
    }

    public void setUpdate_by(String update_by) {
        this.update_by = update_by;
    }


}
