/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lizks.tv.object;

import lizks.tv.util.Config;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class SystemData {

    private static final Logger logger = Logger.getLogger(SystemData.class);
    private static final Map<String, String> strLocalConfig = new HashMap<>();
    private static CompositeConfiguration config;
    public static String SERIAL_NUMBER = "";
    public static String JWT_TOKEN = "";



    static {
        try {
            String configFile = Config.getParam("config", "nas_config");
            File nasConfigFile = new File(configFile);
            if (nasConfigFile.exists()) {
                config = new CompositeConfiguration();
                config.addConfiguration(new HierarchicalINIConfiguration(configFile));
                SERIAL_NUMBER = getParam("Basic", "SerialNumber");
            } else {
                logger.error("Configuration file doen't exists.");
            }

            logger.info("Get setting");

            

        } catch (ConfigurationException ex) {
            System.out.println("Exception when Config");
            System.exit(1);
        }
    }

    public static String getParam(String section, String param) {
        String key = section + "." + param;
        String value;
        if (strLocalConfig.containsKey(key)) {
            value = strLocalConfig.get(key);
        } else {
            value = config.getString(key, "");
            strLocalConfig.put(key, value);
        }
        return value;
    }

    public static String getNasConfig() {
        return "";
    }

}
