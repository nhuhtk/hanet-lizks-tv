package lizks.tv.sever;

import lizks.tv.controller.LizksTVController;
import lizks.tv.util.Config;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.DefaultHandler;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletHandler;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author hoang
 * created on 8/4/2020
 * inside the package - lizks.tv.sever
 */
public class RestServer implements Runnable {
    private static final Logger logger = Logger.getLogger(RestServer.class);
    private static RestServer instance;
    private Server server = new Server();
    private static final Lock lock = new ReentrantLock();

    public static RestServer getInstance(){
        if(instance == null){
            lock.lock();
            try{
                instance = new RestServer();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                lock.unlock();
            }
        }
        return instance;
    }

    @Override
    public void run() {
        try{
            int port = Config.getIntParam("rest_server","port_listen");
            logger.info("Port: "+port);

            ServerConnector serverConnector = new ServerConnector(server);
            serverConnector.setPort(port);
            serverConnector.setIdleTimeout(10000);
            server.setConnectors(new Connector[]{serverConnector});



            ServletHandler servletHandler = new ServletHandler();
            servletHandler.addServletWithMapping(LizksTVController.class,"/*");
            HandlerList handlers = new HandlerList();
            handlers.setHandlers(new Handler[]{servletHandler, new DefaultHandler()});
            server.setHandler(handlers);
            server.start();
            server.join();



        }catch (Exception exception){
            logger.error("Cannot start rest server: "+exception.getMessage());
        }
    }
    public void stop() throws Exception {
        server.stop();
    }
}
