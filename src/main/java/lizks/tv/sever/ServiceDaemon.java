package lizks.tv.sever;

import org.apache.log4j.Logger;

/**
 * Nhuhoang
 * created on 8/4/2020
 * inside the package - lizks.tv.sever
 */
public class ServiceDaemon {
    private static final Logger logger = Logger.getLogger(ServiceDaemon.class);
    private static RestServer restSever = null;

    public static void main(String[] args) {
        try {
            logger.info("MAIN START...");
            //start rest server
            restSever = RestServer.getInstance();
            new Thread(restSever).start();
            // Start worker
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        logger.info("Shutdown thread before restserver getinstance");
                        if (restSever != null) {
                            restSever.stop();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, "Stop Jetty Hook"));

        } catch (Throwable e) {
            String msg = "Exception encountered during startup.";
            logger.error(msg, e);
            logger.error("Uncaught exception: " + e.getMessage());
            System.exit(3);
        }
    }
}
