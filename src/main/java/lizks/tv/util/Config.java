/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lizks.tv.util;


import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dathn
 */
public class Config {

    private static final Logger logger = Logger.getLogger(Config.class);

    private static final Map<String, String> strLocalConfig = new HashMap<>();
    private static final Map<String, Integer> intLocalConfig = new HashMap<>();
    private static final Map<String, List> lstLocalConfig = new HashMap<>();
    private static CompositeConfiguration config;

    public static final String CONFIG_HOME = "./conf";
    public static final String CONFIG_FILE = "config.ini";

    static {
        try {
            String conf = System.getProperty("configuration");
            File configFile;
            if (conf != null) {
                configFile = new File(conf);
            } else {
                String env = System.getProperty("appenv");
                env = env == null ? "development." : env + ".";
                configFile = new File(CONFIG_HOME + File.separator + env + CONFIG_FILE);
            }
            if (configFile.exists()) {
                config = new CompositeConfiguration();
                config.addConfiguration(new HierarchicalINIConfiguration(configFile));
            } else {
                logger.error("Configuration file doen't exists.");
            }
        } catch (ConfigurationException ex) {
            System.out.println("Exception when Config");
            System.exit(1);
        }
    }

    public static String getParam(String section, String param) {
        String key = section + "." + param;
        String value;
        if (strLocalConfig.containsKey(key)) {
            value = strLocalConfig.get(key);
        } else {
            value = config.getString(key, "");
            strLocalConfig.put(key, value);
        }
        return value;
    }

    public static String getStrParam(String section, String param) {
        String key = section + "." + param;
        String value;
        if (strLocalConfig.containsKey(key)) {
            value = strLocalConfig.get(key);
        } else {
            value = config.getString(key, "");
            strLocalConfig.put(key, value);
        }
        return value;
    }

    public static int getIntParam(String section, String param) {
        String key = section + "." + param;
        int value;
        if (intLocalConfig.containsKey(key)) {
            value = intLocalConfig.get(key);
        } else {
            value = config.getInt(key, 0);
            intLocalConfig.put(key, value);
        }
        return value;
    }

    public static List getListParam(String section, String param) {
        String key = section + "." + param;
        List value;
        if (lstLocalConfig.containsKey(key)) {
            value = lstLocalConfig.get(key);
        } else {
            value = config.getList(key, new ArrayList<>());
            lstLocalConfig.put(key, value);
        }
        return value;
    }
}
