package lizks.tv.util;

/**
 * @author hoang
 * created on 8/5/2020
 * inside the package - lizks.tv.util
 */
public class Constant {
    public static String SETTING_HOT_SINGER = "singer_hot";
    public static String SETTING_HOT_COLLECTION = "collection_hot";
    public static String SETTING_HOT_SONG = "song_hot";
    public static String SETTING_HOT_GENRE = "genre_hot";
    public static String PREFIX_REGIX = ",";
    public static String PREFIX_MP4 = ".mp4";
    public static String PREFIX_JPEG = ".jpg";
    public static String LINK_IMAGE_SINGER = "http://images.hanet.com/img_singers/";
    public static String LINK_IMAGE_SONG = "http://images.hanet.com/img_songs/";
    public static String LINK_IMAGE_GENRE = "http://images.hanet.com/img_genre/";
    public static String LINK_IMAGE_COLLECTION = "http://images.hanet.com/img_collection/";
    public static String PARAM_PATH = "p";
    public static String PARAM_NAME_SINGER= "s_n";
    public static String PARAM_THUMBNAIL= "t";
    public static String PARAM_ID= "i";
    public static String PARAM_NAME= "n";
    public static String PARAM_SONG= "songs";
    public static String PARAM_SINGER= "singers";
    public static String PARAM_GENRE= "genres";
    public static String PARAM_COLLECTION= "collections";
    public static String PARAM_SINGER_ID= "singerId";
    public static String PARAM_QUERY_SEARCH= "keyword";
    public static String PARAM_GENRE_ID= "genreId";
    public static String PARAM_COLLECTION_ID= "collectionId";
    public static String PARAM_PATH_SINGER= "url_si";
    public static String PARAM_PATH_SONG= "url_so";
    public static String PARAM_PATH_GENRE= "url_ge";
    public static String PARAM_PATH_COLLECTION= "url_co";
    public static String STATUS_OK= "OK";
    public static String STATUS_SUCCESS= "Thanh cong";

    public static String PARAM_SIGN= "sign";
    public static String PARAM_DEVICE_ID= "deviceId";
    public static String PARAM_PAGE= "page";
    public static String PARAM_LIMIT= "limit";
    public static String PARAM_TIMESTAMP= "ts";
    public static String REDIS_KEY_LIST_HOT_COLLECTION= "collection_hot_list";
    public static String REDIS_KEY_LIST_HOT_SONG= "song_hot_list";
    public static String REDIS_KEY_LIST_HOT_SINGER= "singer_hot_list";
    public static String REDIS_KEY_LIST_HOT_GENRE= "genre_hot_list";
    public static String REDIS_KEY_GET_SONG_BY_ID = "song_by_id:";
    public static String REDIS_KEY_LIST_SONG_BY_SINGER= "song_by_singer:";
    public static String REDIS_KEY_LIST_SONG_BY_GENRE= "song_by_genre:";
    public static final int REDIS_EXPIRE_TIME_IN_DAY = 1;
}
