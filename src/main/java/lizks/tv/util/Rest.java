/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lizks.tv.util;


import com.google.gson.Gson;
import lizks.tv.object.SystemData;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.UnsupportedCharsetException;
import java.util.HashMap;

/**
 *
 * @author ngotanndieu
 */
public class Rest {

    private static final Logger logger = Logger.getLogger(Rest.class);

    public enum METHOD {
        GET, POST, PUT, DELETE
    }

    private static String _callRESTApi(String url, METHOD method, String data) {
        String rs = "";
        try {
            RequestConfig conf = RequestConfig.custom()
                    .setSocketTimeout(90000)
                    .setConnectTimeout(90000)
                    .setConnectionRequestTimeout(90000)
                    .build();
            CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(conf).build();
            HttpUriRequest req;
            switch (method) {
                case POST:
                    req = new HttpPost(url);
                    if(!data.equalsIgnoreCase("")) {
                        ((HttpPost) req).setEntity(new StringEntity(data, ContentType.APPLICATION_JSON));
                    }
                    if (!SystemData.JWT_TOKEN.equalsIgnoreCase("")) {
                        Header tokenHeader = new BasicHeader("authorization", SystemData.JWT_TOKEN);
                        ((HttpPost) req).setHeader(tokenHeader);
                    }
                    break;
                case PUT:
                    req = new HttpPut(url);
                    ((HttpPut) req).setEntity(new StringEntity(data, "UTF-8"));
                    break;
                case DELETE:
                    req = new HttpDelete(url);
                    break;
                default:
                    req = new HttpGet(url);
                    break;
            }
            try (CloseableHttpResponse res = client.execute(req)) {
                HttpEntity entity = res.getEntity();
                InputStream inputStream = entity.getContent();
                rs = IOUtils.toString(inputStream, "UTF-8");
                logger.info("");
                logger.info("* url: " + url);
                logger.info("* data: " + data);
                logger.info("* resp: " + rs);
            }
        } catch (UnsupportedCharsetException | IOException | UnsupportedOperationException ex) {
            logger.error("callRESTApi >> ex:", ex);
        }
        return rs;
    }
    
    public static DataResp post(String url, String data) {
        try {
            String resp = _callRESTApi(url, METHOD.POST, data);
            if(resp != null) {
                DataResp fromJson = new Gson().fromJson(resp, DataResp.class);
                return fromJson;
            }
        } catch (Exception e) {
            logger.error("url: " + url);
            logger.error(e.getMessage(), e);
        }
        return null;
    }
    
    public static DataResp post(String url) {
        try {
            String resp = _callRESTApi(url, METHOD.POST, "");
            if(resp != null) {
                DataResp fromJson = new Gson().fromJson(resp, DataResp.class);
                return fromJson;
            }
        } catch (Exception e) {
            logger.error("url: " + url);
            logger.error(e.getMessage(), e);
        }
        return null;
    }
    
    public static DataResp get(String url, String data) {
        String _callRESTApi = _callRESTApi(url, METHOD.GET, data);
        try {
            DataResp fromJson = new Gson().fromJson(_callRESTApi, DataResp.class);
            return fromJson;
        } catch (Exception e) {
            logger.error(e.getMessage() + " url " + url + "_callRESTApi : " + _callRESTApi, e);
        }
        return null;
    }

    public static DataResp call(String url, METHOD method, String data) {
        String _callRESTApi = _callRESTApi(url, method, data);
        try {
            DataResp fromJson = new Gson().fromJson(_callRESTApi, DataResp.class);
            return fromJson;
        } catch (Exception e) {
            logger.error(e.getMessage() + " url " + url + "_callRESTApi : " + _callRESTApi, e);
        }
        return null;
    }

    public static class DataResp {

        private HashMap data;
        private String msg;
        private String err;

        public HashMap getData() {
            return data;
        }

        public void setData(HashMap data) {
            this.data = data;
        }

        public String getMessage() {
            return msg;
        }

        public void setMessage(String msg) {
            this.msg = msg;
        }

        public String getStatus() {
            return err;
        }

        public void setStatus(String status) {
            this.err = status;
        }

        @Override
        public String toString() {
            return new Gson().toJson(this);
        }
    }
}
