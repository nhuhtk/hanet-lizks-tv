/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lizks.tv.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author viettd
 */
public class Utils {

    private static final Logger logger = Logger.getLogger(Utils.class);

    private static final String STATUS_FAIL = "FAIL";
    private static final String STATUS_OK = "OK";

    public static final String API_DOMAIN_V2 = Config.getParam("path", "domain_api");

    public static final byte HEADER_HTML = 0;
    public static final byte HEADER_JS = 1;

    public static void out(String content, HttpServletResponse respon) throws IOException {
        PrintWriter out = respon.getWriter();
        out.print(content);
    }

    public static String buildRespFail(Object data, String message) {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("status", STATUS_FAIL);
        map.put("lizks/tv/object", data);
        map.put("message", message);
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static String buildRespInvalidRequest() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("status", STATUS_FAIL);
        map.put("lizks/tv/object", null);
        map.put("message", "Request không hợp lệ");
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static String buildRespOK(Object data) {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("status", STATUS_OK);
        map.put("lizks/tv/object", data);
        map.put("message", "Thành công");
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static String buildRespUnsupport() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("status", STATUS_FAIL);
        map.put("lizks/tv/object", null);
        map.put("message", "Không tìm thấy api này");
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static String buildRespUnauthorize() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("status", STATUS_FAIL);
        map.put("lizks/tv/object", null);
        map.put("message", "Chuỗi sign không đúng");
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static void logRequest(HttpServletRequest request, Logger logger) {
        logger.info(request.getRequestURI() + "?" + request.getQueryString() + " data >> " + request.getParameterMap() + " ip >>" + getClientIp(request));
    }

    public static String convertDateToString(Date date, String format) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        if (date == null) {
            return dateFormat.format(new Date(Long.MIN_VALUE));
        }
        return dateFormat.format(date);
    }

    public static <E> List<E> toList(Iterable<E> iterable) {
        if (iterable instanceof List) {
            return (List<E>) iterable;
        }
        ArrayList<E> list = new ArrayList<>();
        if (iterable != null) {
            for (E e : iterable) {
                list.add(e);
            }
        }
        return list;
    }

    public static long longValue(Object value) {
        return (value instanceof Number ? ((Number) value).longValue() : -1);
    }

    public static double doubleValue(Object value) {
        return (value instanceof Number ? ((Number) value).doubleValue() : -1.0);
    }

    public static String md5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes("UTF-8"));
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            logger.error("md5 >> ex:", ex);
            return null;
        }
    }

    public static String md5(File input) {
        try {
            FileInputStream fis = new FileInputStream(input);
            String md5 = DigestUtils.md5Hex(fis);
            fis.close();
            return md5;
        } catch (Exception ex) {
            logger.error("md5 >> ex:", ex);
            return null;
        }
    }

    public static String parsePostData(HttpServletRequest request) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            return "";
        }
        return sb.toString();
    }

    public static String buildSigSHA256(Object[] input, String spliter, String key) {
        String result = "";
        if (spliter == null) {
            spliter = "";
        }
        if (key == null) {
            key = "";
        }
        for (Object obj : input) {
            if (obj == null) {
                obj = "";
            }
            result += spliter + String.valueOf(obj);
        }
        result = sha256(result.substring(spliter.length()) + spliter + key);
        return result;
    }

    public static String sha256(String input) {
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            logger.error("sha256 >> ex:", ex);
        }
        return hexString.toString();
    }

    public static boolean authenSha256(Object[] inputs, String secretKey, String sig) {
        String input = "";
        for (Object obj : inputs) {
            input += String.valueOf(obj);
        }
        input += secretKey;
        String mySig = sha256(input);
        return mySig.equalsIgnoreCase(sig);
    }

    public static boolean authenSha256(Object[] inputs, String secretKey, String spliter, String sig) {
        if (!"true".equals(Config.getParam("config", "use_sign"))) {
            return true;
        }
        String input = "";
        for (Object obj : inputs) {
            input += String.valueOf(obj) + spliter;
        }
        input += secretKey;
        String mySig = sha256(input);
        return mySig.equalsIgnoreCase(sig);
    }

    public static String sha1(String input) {
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            logger.error("sha1 >> ex:", ex);
        }
        return hexString.toString();
    }

    public static boolean authenSha1(Object[] inputs, String secretKey, String sig) {
        String input = "";
        for (Object obj : inputs) {
            input += String.valueOf(obj);
        }
        input += secretKey;
        String mySig = sha1(input);
        return mySig.equalsIgnoreCase(sig);
    }

    public static String getClientIp(HttpServletRequest req) {
        String clientip = "";
        if (req.getHeader("HTTP_X_FORWARDED_FOR") != null) {
            clientip = req.getHeader("HTTP_X_FORWARDED_FOR");
        } else if (req.getHeader("X-Forwarded-For") != null) {
            clientip = req.getHeader("X-Forwarded-For");
        } else if (req.getHeader("REMOTE_ADDR") != null) {
            clientip = req.getHeader("REMOTE_ADDR");
        }
        if ("".equals(clientip)) {
            clientip = req.getRemoteAddr();
        }
        return clientip;
    }

    public static boolean isNullOrEmpty(String input) {
        return (input == null || "".equals(input));
    }

    public static String getMyIP() {
        String ip = "";
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }
                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    ip = addr.getHostAddress();
                    if (!ip.contains(":")) {
                        return ip;
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("getMyIP >> ex:", ex);
        }
        return ip;
    }

    public static Date convertStringToDate(String date, String format) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        if (date.equals("")) {
            return null;
        }
        return dateFormat.parse(date);
    }

    public static String formatLong(long value) {
        DecimalFormat formater = new DecimalFormat();
        DecimalFormatSymbols mySymbols = new DecimalFormatSymbols();
        //mySymbols.setDecimalSeparator('.');
        mySymbols.setGroupingSeparator('.');
        formater.setDecimalFormatSymbols(mySymbols);
        return formater.format(value);
    }

    public static int getFileLength(String videoUrl) {
        int size = -1;
        try {
            URL url = new URL(videoUrl);
            URLConnection conn = url.openConnection();
            size = conn.getContentLength();
            if (size < 0) {
                logger.error("Could not determine file size.");
            }
            conn.getInputStream().close();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return size;
    }

    public static long getLastModified(String videoUrl) {
        long size = -1;
        try {
            URL url = new URL(videoUrl);
            URLConnection conn = url.openConnection();
            size = conn.getLastModified();
            if (size < 0) {
                logger.error("Could not determine file size.");
            }
            conn.getInputStream().close();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        return size;
    }

    public static long startOfDay(long ts) {
        ts = ts + 7 * 3600000;
        return ts - (ts % 86400000) - 7 * 3600000;
    }

    public static boolean checkExisted(String urlStr) {
        int code = 404;
        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            code = httpConnection.getResponseCode();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return 200 == code;
    }

    public static int startOfDay(int ts) {
        ts = ts + 7 * 3600;
        return ts - (ts % 86400) - 7 * 3600;
    }


    public static void prepareHeader(HttpServletResponse resp, byte type) {
        resp.setCharacterEncoding("utf-8");
        if (type == HEADER_HTML) {
            resp.setContentType("text/html; charset=utf-8");
        } else if (type == HEADER_JS) {
            resp.setContentType("application/json; charset=utf-8");
        }
        String appName = Config.getParam("static", "app_name");
        resp.addHeader("Server", appName);
    }
    public static String respNotFound() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", -1);
        map.put("msg", "Request Not Found");
        obj.putAll(map);
        return obj.toJSONString();
    }

    public static String respParamInvalid() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", -1);
        map.put("msg", "Request Param Invalid");
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String respFail() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", -1);
        map.put("msg", "Request Fail");
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String respSignKeyInvalid() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", -1);
        map.put("msg", "Sign key invalid");
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String toJSON(int err, String msg) {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", err);
        map.put("msg", msg);
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String toJSON(String status, String msg, Object data) {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", status);
        map.put("msg", msg);
        map.put("data", data);
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String respOK(Object data) {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", 0);
        map.put("msg", "thanh cong");
        map.put("data", data);
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String respInternalError() {
        Map map = new HashMap();
        JSONObject obj = new JSONObject();
        map.put("err", -1);
        map.put("msg", "Internal Error");
        obj.putAll(map);
        return obj.toJSONString();
    }
    public static String genSignKey(String deviceId, String timestamp, String param){
        if(param.isEmpty()){
            return md5(deviceId + timestamp );
        }
        return md5(deviceId + timestamp + md5(param));
    }
    public static String createRequestLog(HttpServletRequest request, HttpServletResponse response, String content)  {
        JSONObject log = new JSONObject();
        JSONObject req = new JSONObject();
        req.put("URI", request.getRequestURI());
        req.put("Query", request
                .getQueryString() != null ? "?" + request.getQueryString().replace("\\", "") : "");

        JSONObject jsonReqMap = requestParamsToJSON(request);
        if (jsonReqMap != null) {
            req.put("Param", jsonReqMap);
        }
        log.put("ts", System.currentTimeMillis());
        log.put("request", req);
        log.put("response", response.toString().replace("\n", " ||| ") + content);
        //
        return log.toString();
    }
    private static JSONObject requestParamsToJSON(HttpServletRequest req) {
        try {
            JSONObject jsonObj = new JSONObject();
            Map<String, String[]> params = req.getParameterMap();
            for (Map.Entry<String, String[]> entry : params.entrySet()) {
                String v[] = entry.getValue();
                Object o = (v.length == 1) ? v[0] : v;
                jsonObj.put(entry.getKey(), o);
            }
            return jsonObj;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }
    public static void main(String[] args) {
//        String _callRESTApi = _callRESTApi("https://graph.facebook.com/v2.6/me/messages?access_token=EAALYjix7Y4wBAB2EprEAk2kOZAL7l3vZAoQ5jePa6W4ZB1oVAnbpYnSed1I0XpsRObIvjTR3EGDUg9gKZB4yRJqZCSzNaZCXlGTPqZByXKZCRUAkg7By4nVLJrIiHJdC3jmVV7WmbwZAegDUrfk3xa6lZBlocuastO05CGVj3JjkgYqgZDZD",
//                METHOD.POST, "{\"recipient\":{\"id\":\"1445114402222204\"},\"message\":{\"text\":\"đạt\"}}");
//        System.out.println(_callRESTApi);
//        String md5 = Utils.md5(new File("/volume1/hanet/media/song_datas/mlacvi.mp4"));
//        System.out.println(md5);
//        md5 = Utils.md5(new File("/volume1/hanet/media/song_datas/i5fxpw.mp4"));
//        System.out.println(md5);
//        md5 = Utils.md5(new File("/volume1/hanet/media/song_datas/q9zuaf.mp4"));
//        System.out.println(md5);
//        md5 = Utils.md5(new File("/volume1/hanet/media/song_datas/cyowet.mp4"));
//        System.out.println(md5);
//        md5 = Utils.md5(new File("/volume1/hanet/media/song_datas/2emey0.mp4"));
//        System.out.println(md5);
    }


}
